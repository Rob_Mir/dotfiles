#
# ~/.bashrc
#

# If not running interactively, don't do anything
# catch non-bash and non-interactive shells
[[ $- == *i* && $BASH_VERSION ]] && SHELL=/bin/bash || return 0

export TERMINAL="st-256color"
export EDITOR="nvim"
export PAGER="less"

# set some defaults
export MANWIDTH=90
export HISTSIZE=10000
export HISTIGNORE="q:f:v"
set -o vi
set -o notify

shopt -s direxpand
shopt -s checkhash
shopt -s sourcepath
shopt -s expand_aliases
shopt -s autocd cdspell
shopt -s extglob dotglob
shopt -s no_empty_cmd_completion
shopt -s autocd cdable_vars cdspell
shopt -s cmdhist histappend histreedit histverify
[[ $DISPLAY ]] && shopt -s checkwinsize

# source shell configs
for f in "$HOME/.bash/"*?.bash; do
    . "$f"
done

# System Info Fetcher
#al-info

PS1='[\u@\h \W]\$ '
