#!/bin/bash
alias entry='nvim ~/Documents/Journal/$(date +"%d-%m-%Y")'
alias ls='ls --color'
alias la='ls --color -Ah'
alias ll='ls --color -lAh'
alias l.='ls --color -ld .*'

alias mkdir='mkdir -pv'
alias grep='grep --color=auto'
alias debug="set -o nounset; set -o xtrace"
alias x='chmod +x'

alias du='du -kh'
alias df='df -kTh'

if hash nvim >/dev/null 2>&1; then
    alias vim='nvim'
    alias v='nvim'
    alias sv='sudo nvim'
else
    alias v='vim'
    alias sv='sudo vim'
fi
alias ipi="ip -br -c a | awk '{ print $3 }'" #internal IP
alias ipe="curl ipinfo.io/ip && echo ''"     #external IP
alias f='ranger'
alias anicli='ani-cli'
alias s="speedtest"
alias wr="curl wttr.in/Tijuana"
alias nb='newsboat'
alias nm="neomutt"
alias yt='pipe-viewer --colors'
alias ytc='pipe-viewer -sc --colors'
alias parrot="curl parrot.live"
alias addcon='/usr/bin/git --git-dir=$HOME/Documents/dotfiles --work-tree=$HOME'

alias gin='git init'
alias ga='git add'
alias gaa='git add .'
alias gp='git pull'
alias gf='git fetch'
alias gc='git clone'
alias gs='git stash'
alias gst='git status'
alias gb='git branch'
alias gm='git merge'
alias gcb='git checkout -b'
alias gch='git checkout'
alias gcm='git commit -m'
alias glg='git log --stat'
alias gpo='git push origin HEAD'
alias gwch='git whatchanged -p --abbrev-commit --pretty=medium'

alias sums='sudo pacman -Syu'
alias sars='sudo pacman -Sc'
alias psp='pacman -Ss'        # search for program
alias pin='sudo pacman -S'    # install
alias pun='sudo pacman -Rs'   # remove
alias pcc='sudo pacman -Scc'  # clear cache
alias pls='pacman -Ql'        # list files
alias prm='sudo pacman -Rnsc' # really remove, configs and all

alias pkg='makepkg --printsrcinfo > .SRCINFO && makepkg -fsrc'
alias spkg='pkg --sign'

alias mk='make && make clean'
alias smk='sudo make clean install && make clean'
alias ssmk='sudo make clean install && make clean && rm -iv config.h'

alias mpv='devour mpv'
alias zathura='devour zathura'

# aliases inside tmux session
if [[ $TERM == *tmux* ]]; then
    alias :sp='tmux split-window'
    alias :vs='tmux split-window -h'
fi

alias rcp='rsync -v --progress'
alias rmv='rcp --remove-source-files'

alias calc='python -qi -c "from math import *"'
alias brok='sudo find . -type l -! -exec test -e {} \; -print'
alias timer='time read -p "Press enter to stop"'

# shellcheck disable=2142
# alias xp='xprop | }'"
alias get='curl --continue-at - --location --progress-bar --remote-name --remote-time'
