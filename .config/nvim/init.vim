let mapleader = " "
" ---------- VimPLug plugins ----------
call plug#begin('~/.local/share/nvim/plug')
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'junegunn/goyo.vim'
Plug 'junegunn/limelight.vim'
Plug 'ap/vim-css-color'
Plug 'neoclide/coc.nvim'
call plug#end()
" -------------------------------------

" Basic configurations
set title
set clipboard+=unnamedplus
set noruler
set nocompatible
filetype plugin on
syntax on
set encoding=utf-8
set number relativenumber
"set laststatus=0
"set noshowcmd
"set noshowmode
"set bg=dark

" Enables autocompletion
set wildmode=longest,list,full

" Disables autocommenting on newline
autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o

" Pluggin Shortcuts
" Goyo, centers text with no distracction
map <leader>w :Goyo <CR>
" Limelight, focuses light on paragraph
map <leader>f :Limelight!! <CR>
let g:limelight_conceal_ctermfg = 'gray' "required for current Cyberpunk color scheme
let g:limelight_conceal_ctermfg = 240	 "required for current Cyberpunk color scheme

"Splits will open down and right
set splitbelow splitright
"source ~/.config/nvim/themes/cyberpunk-neon.vim
